<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bootstrap Mahasiswa1</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
</head>
<body>
<?php 
    //proses yang terjadi
    //membuat sebuah variabel yang mengambil data dari parameter input halaman biodata
    $npm = $_POST['npm'];
    $nama = $_POST['nama'];
    $jk = $_POST['jk'];
    $alamat= $_POST['alamat'];
    $email= $_POST['email'];
?>
    <header class="py-5 bg-primary">
        <div class="container d-flex justify-content-center">
            <h1 class="display-4 text-white ">Data MAHASISWA1</h1>
        </div>
    </header>
    <br>
    <br>
    <br>
<div class="container col-lg-8">
<table class="table ">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">NPM</th>
      <th scope="col">Nama Mahasiswa</th>
      <th scope="col">Jenis Kelamin</th>
      <th scope="col">Email</th>
      <th scope="col">Alamat</th>
    </tr>
  </thead>
  <tbody>
      <!-- Menampilkan Data sesuai variabel yang dibuat diatas -->
    <tr>
      <th scope="row">1</th>
      <td><?php echo $npm; ?></td>
      <td><?php echo $nama; ?></td>
      <td><?php echo $jk; ?></td>
      <td><?php echo $email; ?></td>
      <td><?php echo $alamat; ?></td>
    </tr>
  </tbody>
</table>
<br><br><br><button type="button" class="btn btn-primary" onclick="history.back();">Back</button>
</div>

<script src="js/bootstrap.min.js"></script>
</body>
</html>